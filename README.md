The Arithmetic-Geometric Mean
======

This is a script for my fourth year project. It computes the arithmetic-geometric mean of two positive numbers and uses it to compute pi. It can work to any desired number of significant figures. It is written in Python and makes heavy use of its *decimal* module.

-----
[rowanparker.com](http://rowanparker.com)