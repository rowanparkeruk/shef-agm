#!/usr/bin/env python3
#
# agm.py - Computes the arithmetic-geometric mean at any precision and uses it
#          to calculate pi.
# Copyright (c) 2013-14 Rowan Parker (rowan at rowanparker dot com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function
from decimal import *
import argparse
import sys

class AGM(object):
	def __init__(self, precision=30, print_agm=True, print_errors=True, print_counts=True, print_steps=False, pi_mode=False):
		if int(precision) < 1:
			self.prec = 30
		else:
			self.prec = int(precision)
		getcontext().rounding = ROUND_HALF_UP
		getcontext().prec = self.prec + 2
		self.print_agm = bool(print_agm)
		self.print_errors = bool(print_errors)
		self.print_steps = bool(print_steps)
		self.print_counts = bool(print_counts)
		self.pi_mode = bool(pi_mode)
	def change_prec(self, precision):
		if int(precision) > 2:
			self.prec = int(precision)
		else:
			if self.print_errors:
				print("FAIL: %s.change_prec(%s) returned false because the new precision was lower than 2." % (self.__class__.__name__, precision))
			return False
		getcontext().prec = self.prec + 2
		return True
	def decimal_validate(self, val):
		try:
			return Decimal(val)
		except InvalidOperation:
			a = val.split('^')
			if len(a) == 2:
				t = [self.decimal_validate(a[0]), self.decimal_validate(a[1])]
				if False in t:
					return False
				else:
					return t[0]**t[1]
			b = val.split('/')
			if len(b) == 2:
				t = [self.decimal_validate(b[0]), self.decimal_validate(b[1])]
				if False in t:
					return False
				else:
					return t[0]/t[1]
			return False
	def start(self, ai, bi):
		if self.print_steps or self.print_counts:
			print("->agm: started", end="\r")
		a = self.decimal_validate(ai)
		b = self.decimal_validate(bi)
		if a > 0 and b > 0:
			if a > b:
				self.a = [a]
				self.b = [b]
			else:
				self.a = [b]
				self.b = [a]
			if self.pi_mode:
				self.csum = [getcontext().power(self.a[-1],2) - getcontext().power(self.b[-1],2)]
			with localcontext() as ctx:
				ctx.prec = self.prec
				self.ap = [+self.a[-1]]
				self.bp = [+self.b[-1]]
			self.finished = False
			self.iterate_print()
			return True
		else:
			if self.print_errors:
				print("FAIL: %s.start(%s, %s) returned false because of an error with a and/or b." % (self.__class__.__name__, ai, bi))
			return False
	def iterate(self):
		if self.finished:
			return False
		if not self.a or not self.b:
			if self.print_errors:
				print("FAIL: %s.iterate() returned false because %s.a and/or %s.b were not set. Have you run %s.start() first?" % (self.__class__.__name__, self.__class__.__name__, self.__class__.__name__))
			return False
		am = (self.a[-1]+self.b[-1])/Decimal(2)
		gm = (self.a[-1]*self.b[-1]).sqrt()
		self.a.append(am)
		self.b.append(gm)
		if self.pi_mode:
			t2k = getcontext().power(2, len(self.a)-1)
			tcsq = (getcontext().power(self.a[-1],2) - getcontext().power(self.b[-1],2))
			self.csum.append(self.csum[-1]+t2k*tcsq)
		with localcontext() as ctx:
			ctx.prec = self.prec
			self.ap.append(+self.a[-1])
			self.bp.append(+self.b[-1])
		self.iterate_print()
		if (self.a[-1] - self.b[-1]).adjusted() <= -self.prec:
			self.finished = True
			return True
	def iterate_print(self):
		if self.print_counts or self.print_steps:
			print("-> i=%i" % (len(self.ap)-1), end=" "*30 + "\r")
		if self.print_steps:
			print("\n\t a: %s\n\t b: %s" % (self.ap[-1], self.bp[-1]))
			if self.pi_mode:
				print("\t\b\bcsum: %s" % self.csum[-1])
	def answer(self):
		if self.finished:
			if self.print_agm:
				if self.print_counts or self.print_steps:
					print("\n  agm: ", end="")
				print(self.ap[-1])
				if self.print_steps:
					print(" prec: " + str(self.prec))
			return self.ap[-1]
		else:
			if self.print_errors:
				print("FAIL: %s.answer() returned false because %s.finished is false (the algorithm has not finished)." % (self.__class__.__name__, self.__class__.__name__))
			return False
	def calculate(self, a, b):
		if not self.start(a, b):
			return False
		while not self.iterate():
			pass
		return self.answer()

class Pi(object):
	def __init__(self, print_pi=True, print_errors=True, print_counts=True, print_steps=False, print_agm_steps=False):
		self.print_pi = bool(print_pi)
		self.print_errors = bool(print_errors)
		self.print_steps = bool(print_steps)
		self.print_counts = bool(print_counts)
		self.print_agm_steps = bool(print_agm_steps)
		self.agm = AGM(3, False, self.print_errors, self.print_counts, self.print_agm_steps, True)
	def digits(self, n, pi=False, M=False):
		if n % 1 != 0 or n < 0:
			return False
		if n == 0:
			return 0
		if not pi:
			pi = Decimal('3.142')
		if not M:
			M = Decimal('0.8472')
		a = 2*(getcontext().ln(pi)-getcontext().ln(M))
		b = (n+4)*getcontext().ln(2)
		c = pi*getcontext().power(2, n+1)
		d = (a+b-c)/getcontext().ln(10)
		return int(-d.to_integral_exact(rounding = ROUND_FLOOR))
	def start(self, n):
		if int(n) < 0:
			if self.print_errors:
				print("FAIL: %s.start(%s) returned false because n was not positive." % (self.__class__.__name__, n))
			return False
		else:
			self.n = int(n)
		guess_digits = self.digits(self.n)
		if guess_digits < 3:
			guess_digits += 3
		getcontext().prec = guess_digits+2
		getcontext().rounding = ROUND_HALF_UP
		if not self.agm.change_prec(guess_digits+2):
			return False
		return self.agm.start(Decimal('1'), (Decimal('1')/Decimal('2')).sqrt())
	def iterate(self):
		self.agm.iterate()
		self.iterate_print()
	def iterate_print(self):
		if self.print_steps:
			if not self.print_agm_steps:
				print()
			print("\tpi: " + str(self.equation()))
			print("\t\b\bprec: " + str(self.correct_digits))
	def equation(self):
		if not self.agm.csum:
			return False
		tn = getcontext().power(self.agm.a[-1] + self.agm.b[-1], 2)/Decimal('2')
		td = 1 - self.agm.csum[-1]
		pi = tn/td
		self.correct_digits = self.digits(self.i, pi, self.agm.a[-1])
		with localcontext() as ctx:
			if self.correct_digits < 3:
				ctx.prec = 3
			else:
				ctx.prec = self.correct_digits
			return +pi
	def answer(self):
		if self.i == self.n:
			pi = self.equation()
			if self.print_pi:
				if self.print_counts or self.print_steps:
					print("\n   pi: ", end="")
				print(pi)
				if self.print_counts or self.print_steps:
					print(" prec: " + str(self.correct_digits))
			return pi
		else:
			if self.print_errors:
				print("FAIL: %s.answer() returned false because %s.i != %s.n (the algorithm has not finished)." % (self.__class__.__name__, self.__class__.__name__, self.__class__.__name__))
			return False
	def calculate(self, n):
		if not self.start(n):
			return False
		self.i = 0
		self.iterate_print()
		for i in range(1, n+1):
			self.i = i
			self.iterate()
		return self.answer()

def main():
	parser = argparse.ArgumentParser(description="A script written to calculate the arithmetic-geometric mean to an arbitrary precision, and use it to calculate pi.")
	subparsers = parser.add_subparsers(help="the script's function")
	agm_parser = subparsers.add_parser('agm', help="calculate the arithmetic-geometic mean - use `agm -h` for more info")
	agm_parser.add_argument("a", help="the first value - use / (a forward slash) for fractions and ^ (a caret) for exponentials")
	agm_parser.add_argument("b", help="the second value - same as first")
	agm_parser.add_argument("-p", "--precision", help="change the precision to the positive integer P from the default value of 30", type=int, default=30, metavar='P')
	agm_parser.add_argument("-v", "--verbose", help="print each step of the agm calculation", action="store_true")
	agm_parser.add_argument("-q", "--quiet", help="surpress all output other than the result (and errors)", action="store_true")
	agm_parser.set_defaults(func=do_agm)
	pi_parser = subparsers.add_parser('pi', help="calculate pi - use `pi -h` for more info")
	pi_parser.add_argument("n", help="the number of iterations to perform (the first iteration is n=0)", type=int)
	pi_parser.add_argument("-v", "--verbose", help="print each step of the pi calculation", action="store_true")
	pi_parser.add_argument("-vv", "--veryverbose", help="print each step of the pi and agm calculation", action="store_true")
	pi_parser.add_argument("-q", "--quiet", help="surpress all output other than the result (and errors)", action="store_true")
	pi_parser.set_defaults(func=do_pi)
	args = parser.parse_args()
	args.func(args)

def do_agm(args):
	if args.quiet:
		args.print_counts = False
		args.verbose = False
	else:
		args.print_counts = True
	agm = AGM(args.precision, True, True, args.print_counts, args.verbose, False)
	agm.calculate(args.a, args.b)

def do_pi(args):
	if args.veryverbose:
		args.verbose = True
	if args.quiet:
		args.print_counts = False
		args.verbose = False
		args.veryverbose = False
	else:
		args.print_counts = True
	pi = Pi(True, True, args.print_counts, args.verbose, args.veryverbose)
	pi.calculate(args.n)

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print("\nInterrupted.")
		exit()
	